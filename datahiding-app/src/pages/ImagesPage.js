import ImageList from "../components/ImageList";
import { encryptedUploadsUrl } from "../services/urls";

export default function ImagesPage() {
  return (
    <div>
      <ImageList />
    </div>
  );
}
