import React, { useState, useEffect } from "react";
import { Button, TextField } from "@material-ui/core";

import { UserService } from "../services/Service";
import { useHistory } from "react-router-dom";

export default function LogoutPage(props) {
  const history = useHistory();

  const { globalState, setGlobalState } = props;

  useEffect(() => {
    localStorage.removeItem("authenticatedUser");
    setGlobalState({
      ...globalState,
      authenticatedUser: null,
    });
    console.log("Logging out");
    history.push("/login");
  }, [history, globalState, setGlobalState]);

  return <div></div>;
}
