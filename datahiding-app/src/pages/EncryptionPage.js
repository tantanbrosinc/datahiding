import React, { useEffect, useState } from "react";
import { Button, TextField, Typography } from "@material-ui/core";
import { DropzoneArea } from "material-ui-dropzone";

import { ImageService } from "../services/Service";
import { useHistory } from "react-router-dom";
import Alert from "@material-ui/lab/Alert";

export default function EncryptionPage(props) {
  const imageService = new ImageService();

  // const { globalState, setGlobalState } = props;

  // const { authenticatedUser } = globalState;

  // const [userId, setUserId] = useState(authenticatedUser ? authenticatedUser._id : ""); // TODO;
  const [imagePayload, setImagePayload] = useState({});

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("authenticatedUser"));
    const userId = user ? user._id : null;

    console.log("ENCRYPT IMAGE");
    console.log({ user, userId });

    setImagePayload({
      ...imagePayload,
      userId,
    });
  }, []);

  const [encrypting, setEncrypting] = useState(false);
  const [success, setSuccess] = useState(false);
  const [error, setError] = useState(false);
  const history = useHistory();

  async function onEncryptImageClicked() {
    setSuccess(false);
    setError(false);

    setEncrypting(true);

    console.log({ imagePayload });
    const response = await imageService.encryptImage(imagePayload);
    console.log({ response });

    if (response) {
      setTimeout(() => {
        setSuccess(true);
        setTimeout(() => {
          history.push("/home");
        }, 2000);
      }, 5000);
    } else {
      setError(true);
    }
  }

  function onRawImageChanged(files) {
    setImagePayload({
      ...imagePayload,
      rawImage: files && files[0],
    });
  }

  function onPayloadChanged(files) {
    setImagePayload({
      ...imagePayload,
      payload: files && files[0],
    });
  }

  return (
    <div className="EncryptionPage" style={{ width: 800, marginLeft: "auto", marginRight: "auto", height: "100vh", marginTop: "3vh" }}>
      <form noValidate autoComplete="off">
        <Typography variant="h6" gutterBottom color="secondary">
          Select Image
        </Typography>

        <DropzoneArea
          id="dropzone-rawImage"
          onChange={e => onRawImageChanged(e)}
          acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
          maxFileSize={5000000}
          filesLimit={1}
        />

        <Typography variant="h6" gutterBottom color="secondary">
          Select Text File
        </Typography>

        <DropzoneArea
          id="dropzone-payload"
          onChange={e => onPayloadChanged(e)}
          acceptedFiles={["text/plain"]}
          maxFileSize={5000000}
          filesLimit={1}
        />

        <Typography variant="h6" gutterBottom color="secondary">
          Enter Secret Passphrase
        </Typography>
        <TextField
          id="secret"
          variant="outlined"
          type="password"
          fullWidth={true}
          required
          onChange={e => setImagePayload({ ...imagePayload, secret: e.target.value })}
        />
        <br />
      </form>
      {encrypting && !success && <Alert severity="info">Please wait while image is being encrypted. This may take 5-10 seconds...</Alert>}
      {success && (
        <Alert severity="success">Image has been successfully encrypted. Please wait while to redirect you to the images page...</Alert>
      )}
      {error && <Alert severity="error">There was a problem in encrypting the image. Please try again</Alert>}
      <Button variant="contained" color="primary" onClick={onEncryptImageClicked}>
        Encrypt Image
      </Button>
    </div>
  );
}
