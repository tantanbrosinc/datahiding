import React, { useState } from "react";
import { Button, TextField, Typography } from "@material-ui/core";

import Alert from "@material-ui/lab/Alert";

import { ImageService } from "../services/Service";
import { useParams } from "react-router-dom";
import { apiUrl } from "../services/urls";

export default function DecryptionPage(props) {
  const params = useParams();
  const { imageId } = params;
  const [password, setPassword] = useState("");
  const [hasResult, setHasResult] = useState(false);

  const [images, setImages] = useState({});
  const [properties, setProperties] = useState({});

  const [invalidPassword, setInvalidPassword] = useState(false);
  const [decrypting, setDecrypting] = useState(false);

  async function onDecryptImage() {
    setDecrypting(true);
    setInvalidPassword(false);

    console.log(`Decrypt Image: ${imageId}`);
    const decryptPayload = {
      imageId,
      secret: password,
    };

    console.log(decryptPayload);

    // // try to download the decrypted file
    const imageService = new ImageService();
    const result = await imageService.decryptImage(decryptPayload);
    if (result) {
      const { urlResultPath, urlOriginalPath, urlEncryptedPath, urlReconstructedPath, properties } = result;
      setHasResult(true);
      setImages({
        urlOriginalPath: `${apiUrl}/${urlOriginalPath}`,
        urlEncryptedPath: `${apiUrl}/${urlEncryptedPath}`,
        urlReconstructedPath: `${apiUrl}/${urlReconstructedPath}`,
      });
      setProperties(properties);

      setDecrypting(false);
      console.log({ urlOriginalPath, urlEncryptedPath, urlReconstructedPath });
      const url = `${apiUrl}/${urlResultPath}`;
      window.open(url);
    } else {
      setInvalidPassword(true);
      setDecrypting(false);
    }
  }

  return (
    <div style={{ maxWidth: 800, marginLeft: "auto", marginRight: "auto", height: "100vh", marginTop: "3vh" }}>
      <Typography variant="h6" gutterBottom color="secondary">
        Enter Secret Passphrase
      </Typography>
      <TextField
        id="outlined-basic"
        variant="outlined"
        onChange={e => setPassword(e.target.value)}
        type="password"
        fullWidth={true}
        style={{ marginBottom: "1vh" }}
      />

      {decrypting && <Alert severity="info">Please wait while the image is being decrypted.</Alert>}
      {invalidPassword && <Alert severity="error">Invalid Password. Please try again</Alert>}

      <Button variant="contained" color="primary" onClick={onDecryptImage}>
        Decrypt Image
      </Button>

      {hasResult && (
        <div style={{ textAlign: "center", display: "block", marginLeft: "auto", marginRight: "auto" }}>
          <div style={{ display: "flex", flexWrap: "wrap", marginLeft: "auto", marginRight: "auto" }}>
            <div style={{ margin: 20, marginLeft: "auto", marginRight: "auto" }}>
              <Typography variant="h6" gutterBottom color="black">
                Original Image
              </Typography>
              <img src={images.urlOriginalPath} alt="Original" />
            </div>
            <div style={{ margin: 20, marginLeft: "auto", marginRight: "auto" }}>
              <Typography variant="h6" gutterBottom color="black">
                Encrypted Image
              </Typography>
              <img src={images.urlEncryptedPath} alt="Encrypted" />
            </div>
            <div style={{ margin: 20, marginLeft: "auto", marginRight: "auto" }}>
              <Typography variant="h6" gutterBottom color="black">
                Reconstructed Image
              </Typography>
              <img src={images.urlReconstructedPath} alt="Reconstructed" />
            </div>
          </div>
          <Typography style={{ display: "flow" }} variant="h6" gutterBottom color="secondary">
            Reconstructed Image Similarity Index: {properties.similarityIndex}
          </Typography>
        </div>
      )}
    </div>
  );
}
