import logo from "./logo.svg";
import "./App.css";
import { Button, TextField } from "@material-ui/core";
import LoginPage from "./pages/LoginPage";
import RegistrationPage from "./pages/RegistrationPage";
import EncryptionPage from "./pages/EncryptionPage";
import DecryptionPage from "./pages/DecryptionPage";
import ImagesPage from "./pages/ImagesPage";
import NavBar from "./components/NavBar";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";
import LogoutPage from "./pages/LogoutPage";
import React, { useEffect, useState } from "react";

import { AppBar, Toolbar, Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import { InputBase } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  bar: {
    backgroundColor: "#222e42",
  },
  title: {
    fontFamily: "Orbitron",
    color: "#3ddbbe",
    fontSize: "30px",
  },
  bg: {
    position: "relative",
    backgroundColor: "#46484d",
    height: "100%",
  },
}));

export default function App() {
  const classes = useStyles();

  const [globalState, setGlobalState] = useState({
    authenticatedUser: null,
  });

  // useEffect(() => {
  //   const user = localStorage.getItem("authenticatedUser");
  //   if (user) {
  //     setGlobalState({
  //       ...globalState,
  //       authenticatedUser: user,
  //     });
  //   }
  // }, [globalState, setGlobalState]);

  return (
    <Router>
      <div className="App">
        <div className={classes.bg}>
          <Paper elevation={20}>
            <Box style={{ background: "linear-gradient(45deg, #a81d59 30%, #9225ba 90%)", height: "10vh" }}>
              <NavBar globalState={globalState} setGlobalState={setGlobalState} />
            </Box>
          </Paper>
          <Switch>
            <Route path="/login">
              <LoginPage globalState={globalState} setGlobalState={setGlobalState} />
            </Route>
            <Route path="/register">
              <RegistrationPage globalState={globalState} setGlobalState={setGlobalState} />
            </Route>
            <Route path="/home">
              <ImagesPage globalState={globalState} setGlobalState={setGlobalState} />
            </Route>
            <Route path="/encrypt">
              <EncryptionPage globalState={globalState} setGlobalState={setGlobalState} />
            </Route>
            <Route path="/decrypt/:imageId">
              <DecryptionPage />
            </Route>

            <Route path="/logout">
              <LogoutPage globalState={globalState} setGlobalState={setGlobalState} />
            </Route>
            <Redirect exact from="/" to="/login" />
          </Switch>
        </div>
      </div>
    </Router>
  );
}
