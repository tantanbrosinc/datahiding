import { Button, Typography } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { ImageService } from "../services/Service";
import { apiUrl } from "../services/urls";

export default function DecryptedItem(props) {
  const decryptedData = "hehe";
  const encryptedImageUrl = "uploads/encrypted/encrypted_1628339364360.jpg";
  const reconstructedImageUrl = "uploads/reconstructed/reconstructed_1628339364361.jpg";
  const similarityIndex = "69";

  return (
    <div>
      {/* <h1>Encrypted Image</h1>
      <img alt="Item" src={encryptedImageUrl} />
			 */}
      <Typography variant="h4" color="textPrimary">
        Decrypted Data: {decryptedData}
      </Typography>

      <h1>Reconstructed Image</h1>
      <img alt="Item" src={`${apiUrl}/${reconstructedImageUrl}`} />

      <Typography variant="h4" color="textPrimary">
        Similarity Index: {similarityIndex}
      </Typography>
    </div>
  );
}
