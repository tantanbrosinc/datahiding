import React, { useEffect, useState } from "react";
import ImageItem from "./ImageItem";
import DecryptedItem from "./DecryptedItem";
import { apiUrl, encryptedUploadsUrl } from "../services/urls";
import UserService from "../services/UserService";
import Alert from "@material-ui/lab/Alert";

export default function ImageList(props) {
  const [images, setImages] = useState([
    // { id: "610e7ca4de6c8961dc36c47f", text: "image 1", url: `${apiUrl}\\uploads\\encrypted\\encrypted_1628339364360.jpg` },
    // { id: "610e7eb9b2f4ec6208db5f17", text: "image2", url: `${apiUrl}\\uploads\\encrypted\\encrypted_1628339897556.jpg` },
  ]);

  // const { globalState, setGlobalState } = props;
  // const userId = globalState.authenticatedUser ? globalState.authenticatedUser._id : null;

  useEffect(() => {
    async function getImages() {
      const user = JSON.parse(localStorage.getItem("authenticatedUser"));
      const userId = user ? user._id : null;

      console.log("GET IMAGES");
      console.log({ user, userId });

      if (userId) {
        const userService = new UserService();
        const responseImages = await userService.getImagesByUserId(userId);

        console.log("GET IMAGES");
        console.log(responseImages);

        const imagesToDisplay = responseImages.map((x, i) => {
          return { id: x._id, text: `Image ${i + 1}`, url: `${apiUrl}\\${x.encryptedImageLocation}` };
        });

        setImages(imagesToDisplay);
      }
    }

    getImages();
  }, []);

  return (
    <div style={{ display: "flex", flexWrap: "wrap" }}>
      {images.length <= 0 && (
        <Alert style={{ width: "100%" }} severity="info">
          You don't have any encrypted images yet. Please go to the encryption page to start using the Application
        </Alert>
      )}
      {images.map((image, i) => (
        <ImageItem key={image.id} id={image.id} text={image.text} url={image.url} />
      ))}
    </div>
  );
}
