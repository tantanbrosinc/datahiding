import React, { useEffect, useState } from "react";
import { Button, TextField } from "@material-ui/core";

import { UserService } from "../services/Service";
import { useHistory } from "react-router-dom";

import { BrowserRouter as Router, Switch, Route, Link, useRouteMatch, useParams } from "react-router-dom";

import { AppBar, Toolbar, Typography, Grid } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import { InputBase } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  bar: {
    backgroundColor: "#222e42",
  },
  title: {
    fontFamily: "Orbitron",
    color: "#3ddbbe",
    fontSize: "30px",
  },
  bg: {
    backgroundColor: "#46484d",
    height: "100%",
    position: "relative",
  },
  ul: {
    display: "inline",
  },
}));

export default function NavBar(props) {
  const classes = useStyles();

  const { globalState, setGlobalState } = props;
  const [authenticated, setAuthenticated] = useState(false);

  console.log("NAVBAR");
  console.log(globalState);

  useEffect(() => {
    const user = JSON.parse(localStorage.getItem("authenticatedUser"));
    const userId = user ? user._id : null;
    setAuthenticated(userId ? true : false);
  }, [globalState, setAuthenticated]);

  return (
    <div>
      <AppBar className={classes.bar}>
        <Toolbar>
          <Typography variant="h5" className={classes.title}>
            Data Hiding
          </Typography>
          {(authenticated && GetAuthenticatedLinks(classes)) || GetUnathenticatedLinks(classes)}
        </Toolbar>
      </AppBar>
    </div>
  );
}

function GetAuthenticatedLinks(classes) {
  return (
    <div style={{ marginLeft: "auto", marginRight: 0 }}>
      <Link to="/home" component={Button} color="primary">
        Home
      </Link>
      <Link to="/encrypt" component={Button} color="primary">
        Encrypt
      </Link>
      <Link to="/logout" component={Button} color="secondary">
        Logout
      </Link>
    </div>
  );
  // return (
  //   <div>
  //     <ul className={classes.ul}>
  //       <li>
  //         <Link to="/Home">Home</Link>
  //       </li>
  //       <li>
  //         <Link to="/encrypt">Encrypt</Link>
  //       </li>
  //       <li>
  //         <Link to="/logout">Logout</Link>
  //       </li>
  //     </ul>
  //   </div>
  // );
}

function GetUnathenticatedLinks() {
  return (
    <div style={{ marginLeft: "auto", marginRight: 0 }}>
      <Link to="/login" component={Button} color="primary">
        Login
      </Link>
      <Link to="/register" component={Button} color="secondary">
        Register
      </Link>
    </div>
  );
  // return (
  //   <div>
  //     <ul>
  //       <li>
  //         <Link to="/login">Login</Link>
  //       </li>
  //       <li>
  //         <Link to="/register">Register</Link>
  //       </li>
  //     </ul>
  //   </div>
  // );
}
