import { Button, Typography } from "@material-ui/core";
import React, { useState, useEffect } from "react";
// import { ImageService } from "../services/Service";
// import { apiUrl } from "../services/urls";

import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    maxHeight: 345,
    margin: 20,
    justifyContent: "center",
    backgroundColor: "#222e42",
  },
});

export default function ImageItem(props) {
  // const imageService = new ImageService();
  const classes = useStyles();

  const [imageId, setImageId] = useState(null);

  useEffect(() => {
    setImageId(props.id);
  }, [props]);

  function onDecryptImage() {
    const url = `/decrypt/${imageId}`;
    window.open(url);
    // console.log(`Decrypt Image: ${imageId}`);
    // const decryptPayload = {
    //   imageId,
    //   secret: "pass123",
    // };

    // try to download the decrypted file
    // const { urlResultPath, urlReconstructedPath, urlPropertiesPath } = await imageService.decryptImage(decryptPayload);
    // if (urlResultPath) {
    //   // const url = `${apiUrl}/${urlResultPath}`;
    //   const url = `/decrypt/${imageId}`;
    //   window.open(url);
    // }
  }

  return (
    <Card className={classes.root}>
      <CardActionArea>
        <CardMedia component="img" alt="Contemplative Reptile" height="200" width="200" title="Contemplative Reptile" src={props.url} />
      </CardActionArea>
      <CardActions>
        <Button size="small" color="primary" onClick={onDecryptImage}>
          Decrypt
        </Button>
        {/* <Button size="small" color="primary">
          Delete
        </Button> */}
      </CardActions>
    </Card>

    // <div>
    //   <Typography variant="h4" color="textPrimary">
    //     {props.text}
    //   </Typography>
    //   <img id={props.id} alt="Item" src={props.url} />
    //   <Button variant="contained" color="primary" onClick={onDecryptImage}>
    //     Decrypt Image
    //   </Button>
    // </div>
  );
}
