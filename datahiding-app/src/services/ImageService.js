import axios from "axios";
import { apiUrl, encryptImageUrl, decryptImageUrl, imagesUrl } from "./urls";

export default class ImageService {
  async encryptImage(imagePayload) {
    try {
      const formData = new FormData();

      formData.append("rawImage", imagePayload.rawImage);
      formData.append("payload", imagePayload.payload);
      formData.append("userId", imagePayload.userId);
      formData.append("secret", imagePayload.secret);

      const response = await axios.post(encryptImageUrl, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      const { success, imageId } = response.data;

      if (success) {
        console.log(`Successfully Encrypted Image: ${imageId}`);
        return imageId;
      } else {
        console.warn("Failed to Encrypt Image");
        return null;
      }
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async decryptImage(decryptPayload) {
    try {
      const response = await axios.post(decryptImageUrl, decryptPayload);
      const { success, urlOriginalPath, urlResultPath, urlEncryptedPath, urlReconstructedPath, properties, message } = response.data;
      console.log(response.data);
      if (success) {
        console.log(`Sucessfully Decrypted Image`);
        return { urlResultPath, urlOriginalPath, urlEncryptedPath, urlReconstructedPath, properties };
      } else {
        console.warn(`Failed to Decrypt Image: ${message}`);
        return null;
      }
    } catch (error) {
      console.error(error);
      return null;
    }
  }
}
