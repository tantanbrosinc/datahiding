export const apiUrl = "http://localhost:3000";
export const usersUrl = `${apiUrl}/users`;
export const authenticateUrl = `${usersUrl}/authenticate`;
export const imagesUrl = `${apiUrl}/images`;
export const encryptImageUrl = `${imagesUrl}/encrypt`;
export const decryptImageUrl = `${imagesUrl}/decrypt`;

export const uploadsUrl = `${apiUrl}/uploads`;
export const rawUploadsUrl = `${uploadsUrl}/raw`;
export const encryptedUploadsUrl = `${uploadsUrl}/encrypted`;
export const decryptedUploadsUrl = `${uploadsUrl}/decrypted`;
