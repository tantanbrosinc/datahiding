import axios from "axios";
import { authenticateUrl, usersUrl } from "./urls";

export default class UserService {
  async createUser(user) {
    try {
      const response = await axios.post(usersUrl, user);
      const { success, message, userId } = response.data;
      if (success) {
        console.log(`Successfully Created User: ${userId}`);
        return userId;
      } else {
        console.warn(`Failed to Create User: ${message}`);
        return null;
      }
    } catch (error) {
      console.error(error);
      return null;
    }
  }

  async getUserById(userId) {
    try {
      const response = await axios.get(`${usersUrl}/${userId}`);
      const user = response.data;
      if (user) {
        console.log(`Successfully Got User Info: ${user._id}`);
        return user;
      } else {
        console.warn(`Failed to Get User`);
      }
    } catch (error) {
      console.error(error);
    }
  }

  async authenticateUser(credentials) {
    try {
      const response = await axios.post(authenticateUrl, credentials);
      const user = response.data;
      if (user) {
        console.log(`Successfully Authenticated User: ${user._id}`);
        return user;
      } else {
        console.warn(`Failed to Get User`);
      }
    } catch (error) {
      console.error(error);
    }
    return null;
  }

  async getImagesByUserId(userId) {
    try {
      console.log("SUERIVSSERVIVEI");
      const response = await axios.get(`${usersUrl}/${userId}/images`);
      console.log(response);
      const { success, images, message } = response.data;
      if (success) {
        return images;
      } else {
        console.warn(`Failed to get images: ${message}`);
      }
    } catch (error) {
      console.error(error);
      return [];
    }
  }
}
