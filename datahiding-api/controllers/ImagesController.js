const mongoose = require("mongoose");
const dto = require("../dto/Dto");
const uuid = require("uuid").v4;

const User = require("../models/User");
const Image = require("../models/Image");

const HashService = require("../lib/services/HashService");
const DataHidingService = require("../lib/services/DataHidingService");
const fs = require("fs");
const os = require("os");
const path = require("path");

exports.encryptImage = async (files, body, res) => {
  try {
    // find user
    const userId = body.userId;
    const isValid = mongoose.Types.ObjectId.isValid(userId);
    if (!isValid) {
      return res.status(404).json(null);
    }
    const user = await User.findById(userId).select(dto.User);
    if (!user) {
      return res.status(404).json(null);
    }

    // create salt and hash for secret
    const { salt, hash } = HashService.generateHashWithSalt(body.secret);

    // embed actual image
    const encryptedImagePath = DataHidingService.embed(files.rawImage[0].path, files.payload[0].path, hash);

    const encryptedImage = new Image({
      rawPayloadLocation: files.payload[0].path,
      rawImageLocation: files.rawImage[0].path,
      encryptedImageLocation: encryptedImagePath,
      secretSalt: salt,
      secretHash: hash,
      owner: user,
    });

    const doc = await encryptedImage.save();
    console.log("Saved", { doc });
    return res.status(200).json({ success: true, message: "Image successfully encrypted", imageId: doc._id });
  } catch (e) {
    console.error(e);
    return res.status(500).json({ success: false, message: "There was an error in the database" });
  }
};

exports.getImagesByUserId = async (userId, res) => {
  try {
    console.log("getImagesByUserId");
    const images = await Image.find({ owner: userId }).select(dto.Image);
    return res.status(200).json({ success: true, images });
  } catch (e) {
    console.error(e);
    return res.status(500).json({ success: false, message: "There was an error in the database" });
  }
};

exports.decryptImage = async (body, res) => {
  try {
    // find image
    const { imageId, secret } = body;
    const image = await Image.findById(imageId);

    // check if secret is valid
    const valid = HashService.verifySecret(secret, image.secretSalt, image.secretHash);
    if (!valid) {
      return res.status(400).json({ success: false, message: "Invalid secret passphrase provided" });
    }

    // extract data from the encrypted image
    const { reconstructedResultPath, propertiesResultPath, decryptedResultPath } = DataHidingService.extract(
      image.encryptedImageLocation,
      image.secretHash,
      image.rawPayloadLocation
    );

    const urlOriginalPath = image.rawImageLocation.replace(/\\/g, "/");
    const urlResultPath = decryptedResultPath.replace(/\\/g, "/");
    const urlEncryptedPath = image.encryptedImageLocation.replace(/\\/g, "/");
    const urlReconstructedPath = reconstructedResultPath.replace(/\\/g, "/");
    const urlPropertiesPath = propertiesResultPath.replace(/\\/g, "/");

    const propertiesPath = path.resolve(urlPropertiesPath);

    let counter = 0;
    const waitFinish = setInterval(() => {
      if (fs.existsSync(propertiesPath)) {
        clearInterval(waitFinish);

        const properties = JSON.parse(fs.readFileSync(propertiesPath));
        console.log({ propertiesPath, properties });

        console.log({ success: true, urlOriginalPath, urlResultPath, urlEncryptedPath, urlReconstructedPath, properties });

        // TODO: yung slashhh to url dapat, pano yon
        return res.status(200).json({ success: true, urlOriginalPath, urlResultPath, urlEncryptedPath, urlReconstructedPath, properties });
      } else {
        counter += 1;
        if (counter > 60) {
          clearInterval(waitFinish);
          return res.status(500).json({ success: false, message: "There was an error in decrypting the image" });
        }
      }
    }, 1000);
  } catch (e) {
    console.error(e);
    return res.status(500).json({ success: false, message: "There was an error in the database" });
  }
};
