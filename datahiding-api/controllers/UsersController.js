const mongoose = require("mongoose");
const User = require("../models/User");
const uuid = require("uuid").v4;
const dto = require("../dto/Dto");

exports.createUser = async (userToCreate, res) => {
  try {
    let user = new User(userToCreate);
    const doc = await user.save();
    console.log("Saved", { doc });
    return res.status(200).json({ success: true, message: "User successfully created", userId: user._id });
  } catch (e) {
    console.error(e);
    if (e.code === 11000) {
      return res.status(409).json({ success: false, message: "Username is already taken" });
    }
    return res.status(500).json({ success: false, message: "There was an error in the database" });
  }
};

exports.getUserById = async (userId, res) => {
  try {
    // just return null if id is not valid, since this is getById method
    const isValid = mongoose.Types.ObjectId.isValid(userId);
    if (!isValid) {
      return res.status(200).json(null);
    }

    const user = await User.findById(userId).select(dto.User);
    return res.status(200).json(user);
  } catch (e) {
    console.error(e);
    return res.status(500).json({ success: false, message: "There was an error in the database" });
  }
};

exports.authenticateUser = async (credentials, res) => {
  try {
    const user = await User.findOne({ username: credentials.username, password: credentials.password }).select(dto.User);
    console.log({ user });
    if (user) {
      return res.status(200).json(user);
    } else {
      return res.status(404).json(null);
    }
  } catch (e) {
    console.error(e);
    return res.status(500).json({ success: false, message: "There was an error in the database" });
  }
};
