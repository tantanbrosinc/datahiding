const express = require("express");
const router = express.Router();
const UsersController = require("../controllers/UsersController");
const ImagesController = require("../controllers/ImagesController");

/** Create User */
router.post("/", (req, res) => UsersController.createUser(req.body, res));

/** Get User by Id */
router.get("/:userId", (req, res) => UsersController.getUserById(req.params.userId, res));

/** Get Images of the user */
router.get("/:userId/images", (req, res) => ImagesController.getImagesByUserId(req.params.userId, res));

/** Authenticate user */
router.post("/authenticate", (req, res) => UsersController.authenticateUser(req.body, res));

module.exports = router;
