const express = require('express');
const router = express.Router();
const multer  = require('multer')
const path = require('path');
const ImagesController = require("../controllers/ImagesController");

// TODO: Create Folder if doesnt exists
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/raw/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
})

const upload = multer({storage: storage});

const uploader = upload.fields([
	{name:'rawImage', maxCount: 1},
	{name:'payload', maxCount: 1}
]);

/** Encrypt Image */
router.post("/encrypt", uploader, (req, res) => ImagesController.encryptImage(req.files, req.body, res));

/** Decrypt Image */
router.post("/decrypt", (req, res) => ImagesController.decryptImage(req.body, res));

module.exports = router;
