const dto = {
	User: "_id firstName lastName username",
	Image: "_id rawPayloadLocation rawImageLocation encryptedImageLocation owner"
};

module.exports = dto;
