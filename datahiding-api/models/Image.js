let mongoose = require("mongoose");
let Schema = mongoose.Schema;
let ImageSchema = new Schema({
	rawPayloadLocation: {
    type: Schema.Types.String
	},
  rawImageLocation: {
    type: Schema.Types.String
  },
  encryptedImageLocation: {
    type: Schema.Types.String
  },
	secretSalt: {
    type: Schema.Types.String,
  },
  secretHash: {
    type: Schema.Types.String,
  },
  owner: {
		type: Schema.Types.ObjectId,
		ref: "User"
	},
});

module.exports = mongoose.model("Image", ImageSchema);
