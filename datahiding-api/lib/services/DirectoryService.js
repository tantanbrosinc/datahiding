const path = require("path");
const fs = require("fs");

exports.createDirectories = () => {
  console.log(`Creating Directory: ${process.env.RAW_UPLOADS_PATH}`);
  const rawUploads = path.resolve(process.env.RAW_UPLOADS_PATH);
  if (!fs.existsSync(rawUploads)) fs.mkdirSync(rawUploads, { recursive: true });

  console.log(`Creating Directory: ${process.env.ENCRYPTED_UPLOADS_PATH}`);
  const encryptedUploads = path.resolve(process.env.ENCRYPTED_UPLOADS_PATH);
  if (!fs.existsSync(encryptedUploads)) fs.mkdirSync(encryptedUploads, { recursive: true });

  console.log(`Creating Directory: ${process.env.DECRYPTED_UPLOADS_PATH}`);
  const decryptedUploads = path.resolve(process.env.DECRYPTED_UPLOADS_PATH);
  if (!fs.existsSync(decryptedUploads)) fs.mkdirSync(decryptedUploads, { recursive: true });

  console.log(`Creating Directory: ${process.env.RECONSTRUCTED_UPLOADS_PATH}`);
  const reconstructedUploads = path.resolve(process.env.RECONSTRUCTED_UPLOADS_PATH);
  if (!fs.existsSync(reconstructedUploads)) fs.mkdirSync(reconstructedUploads, { recursive: true });
};
