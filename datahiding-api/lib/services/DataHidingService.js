const path = require("path");
const fs = require("fs");
const { spawn } = require("child_process");

/**
 *
 * @param {*} rawImagePath
 * @param {*} payloadPath
 * @param {*} password
 * @returns Path to the embedded Image
 */
exports.embed = (rawImagePath, payloadPath, password) => {
  try {
    console.log("Embed", rawImagePath, payloadPath, password);

    const fullRawImagePath = path.resolve(rawImagePath);
    const fullPayloadPath = path.resolve(payloadPath);

    // Copy Raw Image to the encrypted images folder
    const clonedRawImagePath = path.normalize(`uploads/encrypted/encrypted_${path.basename(rawImagePath)}`);
    const fullClonedRawImagePath = path.resolve(clonedRawImagePath);
    fs.copyFileSync(fullRawImagePath, fullClonedRawImagePath);

    console.log(`Embedding to ${fullClonedRawImagePath}`);

    // embed image in place, so clonedRawImagePath is now the embedded image path
    spawnEmbedProcess(fullPayloadPath, fullClonedRawImagePath, password);

    // Path of the encrypted image
    return clonedRawImagePath;
  } catch (e) {
    console.error(e);
    return null;
  }
};

/**
 *
 * @param {*} encryptedImagePath
 * @param {*} password
 * @param {*} rawPayloadLocation
 * @returns Path to the extracted data in a file
 */
exports.extract = (encryptedImagePath, password, rawPayloadLocation) => {
  try {
    console.log("Extract", encryptedImagePath, password);

    // resolve the encrypted image path
    const fullEncryptedImagePath = path.resolve(encryptedImagePath);

    // create filename for file to store extracted results
    const decryptedResultPath = path.normalize(`uploads/decrypted/decrypted_${path.basename(rawPayloadLocation)}`);
    const fullDecryptedResultPath = path.resolve(decryptedResultPath);
    console.log(`Extracting to ${fullDecryptedResultPath}`);

    // create the file first
    fs.writeFileSync(fullDecryptedResultPath, "");

    // Extract Encrypted Data from encrypted image
    spawnExtractProcess(fullEncryptedImagePath, password, fullDecryptedResultPath);

    // Reconstructed Raw Image from encrypted image
    reconstructedPaths = reconstruct(fullEncryptedImagePath, rawPayloadLocation);

    // Path of the extracted data
    return {
      ...reconstructedPaths,
      decryptedResultPath,
    };
  } catch (e) {
    console.error(e);
    return null;
  }
};

function reconstruct(fullEncryptedImagePath, rawPayloadLocation) {
  try {
    const reconstructedResultPath = path.normalize(
      `uploads/reconstructed/reconstructed_${path.basename(rawPayloadLocation, path.extname(rawPayloadLocation))}.jpg`
    );
    const fullReconstructedResultPath = path.resolve(reconstructedResultPath);
    console.log(`Reconstructing to ${fullReconstructedResultPath}`);

    const propertiesResultPath = path.normalize(
      `uploads/reconstructed/properties_${path.basename(rawPayloadLocation, path.extname(rawPayloadLocation))}.json`
    );
    const fullPropertiesResultPath = path.resolve(propertiesResultPath);
    console.log(`Writing Properties to ${fullPropertiesResultPath}`);

    spawnAutoEncoderProcess(fullEncryptedImagePath, fullReconstructedResultPath, fullPropertiesResultPath);

    return {
      reconstructedResultPath,
      propertiesResultPath,
    };
  } catch (e) {
    console.error(e);
    return null;
  }
}

function attachIO(spawnedProcess, type) {
  spawnedProcess.stdout.on("data", data => {
    console.log(`\n[DataHidingService][${type}]: STDOUT`);
    console.log(data.toString());
  });

  spawnedProcess.stderr.on("data", data => {
    console.log(`\n[DataHidingService][${type}]: STDERR`);
    console.log(data.toString());
  });

  spawnedProcess.stdout.on("close", data => {
    console.log(`\n[DataHidingService][${type}]: Process Exited`);
    console.log(data.toString());
  });
}

function spawnEmbedProcess(fullPayloadPath, fullRawImagePath, password) {
  const steghideProcess = path.resolve("./lib/steghide/steghide.exe");
  console.log(`Resolving Path: steghide at ${steghideProcess}`);

  const embedProcess = spawn(steghideProcess, ["embed", "-ef", fullPayloadPath, "-cf", fullRawImagePath, "-p", password]);

  attachIO(embedProcess, "Embed");
}

function spawnExtractProcess(fullEncryptedImagePath, password, decryptedResultPath) {
  const steghideProcess = path.resolve("./lib/steghide/steghide.exe");
  console.log(`Resolving Path: steghide at ${steghideProcess}`);

  const extractProcess = spawn(steghideProcess, [
    "extract",
    "-sf",
    fullEncryptedImagePath,
    "-p",
    password,
    "-xf",
    decryptedResultPath,
    "-f",
  ]);

  attachIO(extractProcess, "Extract");
}

function spawnAutoEncoderProcess(fullEncryptedImagePath, fullReconstructedResultPath, fullPropertiesResultPath) {
  const pythonProcess = path.resolve("./lib/autoencoder/venv/Scripts/python.exe");
  console.log(`Resolving Path: python at ${pythonProcess}`);

  const predictorScript = path.resolve("./lib/autoencoder/Predictor.py");
  console.log(`Predictor at ${predictorScript}`);

  const autoencoderProcess = spawn(pythonProcess, [
    predictorScript,
    fullEncryptedImagePath,
    fullReconstructedResultPath,
    fullPropertiesResultPath,
  ]);

  attachIO(autoencoderProcess, "AutoEncoder");
}
