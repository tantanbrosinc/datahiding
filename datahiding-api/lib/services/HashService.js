const bcrypt = require("bcrypt");

const saltRounds = 10;

exports.generateHashWithSalt = (secret) => {
	const salt = bcrypt.genSaltSync(saltRounds);
	const hash = bcrypt.hashSync(secret, salt);

	return {salt, hash};
}

exports.verifySecret = (secretToVerify, actualSalt, actualHash) => {
	const hashToVerify = bcrypt.hashSync(secretToVerify, actualSalt);
	return hashToVerify === actualHash;
}
