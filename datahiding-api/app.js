require("dotenv").config();
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const cors = require("cors");
var contentDisposition = require("content-disposition");

const database = require("./lib/mongoose/database");
const directoryService = require("./lib/services/DirectoryService");

directoryService.createDirectories();

var app = express();

function setHeaders(res, path) {
  res.setHeader("Content-Disposition", contentDisposition(path));
}

const staticOptions = {
  index: false,
  setHeaders: function (res, path, stat) {
    res.setHeader("Content-Disposition", contentDisposition(path));
  },
};

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use("/public", express.static(path.join(__dirname, "public"), staticOptions));
app.use("/uploads/raw", express.static(path.join(__dirname, process.env.RAW_UPLOADS_PATH), staticOptions));
app.use("/uploads/encrypted", express.static(path.join(__dirname, process.env.ENCRYPTED_UPLOADS_PATH), staticOptions));
app.use("/uploads/decrypted", express.static(path.join(__dirname, process.env.DECRYPTED_UPLOADS_PATH), staticOptions));
app.use("/uploads/reconstructed", express.static(path.join(__dirname, process.env.RECONSTRUCTED_UPLOADS_PATH), staticOptions));

app.use("/users", require("./routes/users"));
app.use("/images", require("./routes/images"));

module.exports = app;
